#include<iostream>
#include<math.h>
#include<fstream>
#include<string>
using namespace std;

////// Read Question File ////////////////
class WrappedBoard{
    public:
        uint16_t Board[9][9];
};
void Read_Sudoku( WrappedBoard *wboard ){
    // Open file
    string filename;
    cout << "Input  the name of question file : ";
    cin >>  filename;
    ifstream infile( filename );
    if(!infile){
        cout << "This file doesn't exist." << endl;
        exit(0);
    }

    // Read value
    uint8_t i,j;
    i=0;
    string line;
    while( getline(infile, line) ){
        for(j=0;j<9;j++){
            if( line[j]-'0' == 0){
                (*wboard).Board[i][j] = 1023;
            }
            else{
                (*wboard).Board[i][j] = pow(2, line[j] - '0');
            }
        }
        i++;
    }
}

////////// Debug Function ///////////////
uint16_t PossibilityToNumber( uint16_t num ){
    uint16_t log;
    log = 0;
    while(1){
        num >>= 1;
        log++;
        if( num & 1 ){ break; }
    }
    return log;
}
void Print_Answer( uint16_t board[9][9] ){
    uint8_t i, j;
    for(i=0;i<9;i++){
        if(i%3==0){cout << "========================================"     << endl;}
        else{cout << "---------------------------------------" << endl;}
        for(j=0;j<9;j++){
            if( (board[i][j]&1)==0 ){
                cout << PossibilityToNumber(board[i][j]);
            }
            else{ cout << 0; }
        
            if(j%3==2){ cout << "  |  ";}
            else{ cout << "   ";}
        }
        cout << endl;
    }
        cout << "========================================" << endl;
    cout << endl;
}
void Print_Board( uint16_t board[9][9] ){
    uint8_t i, j;
    for(i=0;i<9;i++){
        if(i%3==0){cout << "========================================" << endl;}
        else{cout << "---------------------------------------" << endl;}
        for(j=0;j<9;j++){
            cout << board[i][j];
            if(j%3==2){ cout << "  |  ";}
            else{ cout << "   ";}
        }
        cout << endl;
    }
        cout << "========================================" << endl;
    cout << endl;
}
uint8_t Finished_Bool( uint16_t board[9][9] ){
    uint8_t i, j, flag;
    for(i=0;i<9;i++){
        for(j=0;j<9;j++){
            if( board[i][j]&1==1 ){ return 0; }
        }
    }
    return 1;
}

///////////// Solver Part /////////////////
void Sudoku_Solver( WrappedBoard wboard  );

void Reduce_by_Rule( uint16_t board[9][9] );
void Reduce_Row( uint16_t board[9][9], uint8_t square, uint8_t number );
void Reduce_Column( uint16_t board[9][9], uint8_t square, uint8_t number );
void Reduce_Block( uint16_t board[9][9], uint8_t square, uint8_t number );

void Check_Confirmed( uint16_t *number ){
    uint8_t i, flag_num;
    uint16_t dummy; 
    flag_num = 0;
    dummy = *number;
            
    for(i=0;i<16;i++){
        flag_num += ( dummy &1 );
        dummy >>= 1;
    }           
    if( flag_num ==2 ){//If number is confirmed,
        *number ^= 1;//Set to be confirmed
    } 
}
