#include"sudoku.h"

int main(){
    WrappedBoard wBoard;
    Read_Sudoku(&wBoard);
    Print_Answer(wBoard.Board);

    //Solve
    Sudoku_Solver( wBoard );
    
    return 0;
}

uint8_t WhetherChanged( WrappedBoard wboard_1, WrappedBoard wboard_2 ){
    uint8_t i, j;
    for(i=0;i<9;i++){
        for(j=0;j<9;j++){
            if( wboard_1.Board[i][j] != wboard_2.Board[i][j] ){
                return 1;
            }
        }
    }
    return 0;
}

void Sudoku_Solver( WrappedBoard wboard ){
    WrappedBoard wboard_before;
    while( WhetherChanged(wboard_before,wboard) ){
        wboard_before = wboard;
        Reduce_by_Rule( wboard.Board );
    }
    if( Finished_Bool(wboard.Board) ){
        Print_Answer(wboard.Board);
        return;
    }
    else{
        uint8_t square;
        for(square=0;square<81;square++){
            if( (wboard.Board[square/9][square%9]&1)==1 ){
                uint8_t num;
                for(num=1;num<10;num++){ 
                    if( ((wboard.Board[square/9][square%9]>>num)&1)==1 ){
                        uint16_t backup;
                        backup = wboard.Board[square/9][square&9];
                        wboard.Board[square/9][square%9] = (1<<num);
                        Sudoku_Solver( wboard );
                        wboard.Board[square/9][square&9] = backup;
                    }
                }
                break;
            }
        }
    }
}

void Reduce_by_Rule( uint16_t board[9][9] ){
    uint8_t Square, Number;

    for(Square=0;Square<81;Square++){
        if( (board[Square/9][Square%9]&1)==0 ){ continue; }
        for(Number=1;Number<10;Number++){
            if( (board[Square/9][Square%9]>>Number&1)==0 ){ continue; }

            Reduce_Row( board, Square, Number );
            if( (board[Square/9][Square%9]&1)==0 ){ break; }
            if( (board[Square/9][Square%9]>>Number&1)==0 ){ continue; }

            Reduce_Column( board, Square, Number );
            if( (board[Square/9][Square%9]&1)==0 ){ break; }
            if( (board[Square/9][Square%9]>>Number&1)==0 ){ continue; }

            Reduce_Block( board, Square, Number );
            if( (board[Square/9][Square%9]&1)==0 ){ break; }
        } 
    }
}
void Reduce_Row( uint16_t board[9][9], uint8_t square, uint8_t number ){
    uint8_t row, column;
    row = square/9;
    for(column=0;column<9;column++){
        // If (row,column) is confirmed(=0) and (row,column) = "number",
        if( (board[row][column]&1)==0 and (board[row][column]>>number)&1==1){
            board[square/9][square%9] ^= (1 << number);
            Check_Confirmed( &board[square/9][square%9] );
            return;// You can break the function here because reduction occure just onece.
        }
    }
}
void Reduce_Column( uint16_t board[9][9], uint8_t square, uint8_t number ){
    uint8_t row, column;
    column = square%9;
    for(row=0;row<9;row++){
        if( (board[row][column]&1)==0 and (board[row][column]>>number)&1==1){
            board[square/9][square%9] ^= (1 << number);
            Check_Confirmed( &board[square/9][square%9] );
            return;
        }
    }
}
void Reduce_Block( uint16_t board[9][9], uint8_t square, uint8_t number ){
    uint8_t row, column, i, j;
    row = (square/9)/3*3;
    column = (square%9)/3*3;
    for(i=0;i<3;i++){
        for(j=0;j<3;j++){
            if( (row+j-(square/9)==0) and (column+i)-(square%9)==0  ){ continue; }
            if( (board[row+j][column+i]&1)==0 and (board[row+j][column+i]>>number)&1==1){
                board[square/9][square%9] ^= (1 << number);
                Check_Confirmed( &board[square/9][square%9] );
                return;
            }
        }
    }
}

